import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import Home from './components/Home';
import Messages from './components/Messages';
import Search from './components/Search';

const PublicRoutes = () => {
  return (
    <div>
      <Switch>
        <Route 
          exact         
          path={'/profile'}
          component={Home}
        />
        <Route 
          exact         
          path={'/search'}
          component={Search}
        />
        <Route 
          exact         
          path={'/messages'}
          component={Messages}
        />
        <Redirect from="*" to="/profile" />
      </Switch>
    </div>
  );
}


export default PublicRoutes;
