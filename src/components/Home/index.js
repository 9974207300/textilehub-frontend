import React, { Component } from 'react';
import { withStyles, TextField, Button } from '@material-ui/core';
import Sidebar from '../Sidebar';
import { API } from 'aws-amplify';

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%'
  },
});

class Home extends Component {
  constructor() {
    super()
    this.state = {
      user: {},
      inProgess: false
    }
  }

  async componentDidMount() {
    try {
      const user = await API.get('TexHub', '/user/profile');
      this.setState({ user })
    } catch (e) {
      console.log(e);
    }
  }

  handleChange = field => e => {
    const { user } = this.state;
    user[field] = e.target.value;
    this.setState({ user });
  }

  save = async () => {
    const { phone, firstName, lastName } = this.state.user;
    this.setState({ inProgess: true});
    try {
      await API.post('TexHub', '/user/profile', {
        body: {
          phone, firstName, lastName
        }
      });
    } catch (e) {
      console.log(e)
    }

    this.setState({ inProgess: false })
  }

  render() {
    const { classes } = this.props;
    const props = Object.assign({}, this.props);
    delete props.classes;

    return (
      <div className={classes.root}>
        <Sidebar {...props} />
        <div className={classes.content}>
          {
            (this.state.user.email) ? <div>
              <TextField
                label="Email"
                className={classes.textField}
                value={this.state.user.email}
                margin="normal"
                variant="outlined"
                disabled
              />

              <TextField
                label="Phone"
                className={classes.textField}
                value={this.state.user.phone}
                onChange={this.handleChange('phone')}
                margin="normal"
                variant="outlined"
              />

              <TextField
                label="Fisrt Name"
                className={classes.textField}
                value={this.state.user.firstName}
                onChange={this.handleChange('firstName')}
                margin="normal"
                variant="outlined"
              />

              <TextField
                label="Last Name"
                className={classes.textField}
                value={this.state.user.lastName}
                onChange={this.handleChange('lastName')}
                margin="normal"
                variant="outlined"
              />
              <Button color="primary" variant="contained" onClick={this.save}>Save</Button>
              {
                (this.state.inProgess) ?  <p>Saving....</p> : null
              }
            </div> : null

          }
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Home);

