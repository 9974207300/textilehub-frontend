import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import Sidebar from '../Sidebar';
import {
    Table, TableCell, TableRow, TableBody, TableHead, Button, TextField, Modal
} from '@material-ui/core';

import { API } from 'aws-amplify';

function getModalStyle() {
    return {
        top: `35%`,
        left: `35%`,
        transform: `translate(-35%, -35%)`
    }
}

const styles = theme => ({
    root: {
        flexGrow: 1,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
    },
    paper: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
        overflow: 'auto',
        position: 'relative',
        display: 'flex',
        width: '100%',
        flexDirection: 'column'
    },
    categoryChip: {
        margin: '0 10px'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '80%'
    },
});

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
        padding: '16px 56px 16px 24px'
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

class SearchUser extends Component {
    constructor() {
        super()

        this.state = {
            search: '',
            users: [], showModal: false,
            message: '',
            to: null
        }
    }

    handleCancel = () => {
        this.setState({
            showModal: false,
            message: '',
            to: null
        })
    }
    handleChange = name => e => this.setState({ [name]: e.target.value })

    searchUser = async () => {
        try {
            const { search } = this.state;
            const users = await API.post('TexHub', '/user/search', { body: { search } });
            this.setState({ users })
        } catch (e) {
            console.log(e)
        }
    }

    messageUser = user => {
        this.setState({ showModal: true, to: user.username });
    }

    send = async () => {
        try {
            await API.post('TexHub', '/message', { body: {
                message: this.state.message,
                to: this.state.to
            }});
            this.handleCancel();
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const { classes } = this.props;
        const props = Object.assign({}, this.props);
        delete props.classes;

        const { users } = this.state;

        return (
            <div className={classes.root}>
                <Sidebar {...props} />
                <div className={classes.content}>
                    <div>
                        <TextField
                            label="Search"
                            className={classes.textField}
                            value={this.state.search}
                            onChange={this.handleChange('search')}
                            margin="normal"
                            variant="outlined"
                        />

                        <Button color="primary" variant="contained" onClick={this.searchUser}>Search</Button>
                    </div>
                    <div>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <CustomTableCell>Name</CustomTableCell>
                                    <CustomTableCell>Email</CustomTableCell>
                                    <CustomTableCell>Phone</CustomTableCell>
                                    <CustomTableCell>Message</CustomTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    users.map((p, i) => {
                                        return (
                                            <TableRow key={`user${i}`}>
                                                <TableCell>{p.firstName + ' ' + p.lastName}</TableCell>
                                                <TableCell>{p.email}</TableCell>
                                                <TableCell>{p.phone}</TableCell>
                                                <TableCell>
                                                    <Button color="primary" variant="contained" onClick={() => this.messageUser(p)}>Message</Button>
                                                </TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>

                        <Modal
                            open={this.state.showModal}
                            onClose={this.handleCancel}
                        >
                            <div style={getModalStyle()} className={classes.paper}>
                                <TextField
                                    id="outlined-message"
                                    label="Message"
                                    className={classes.textField}
                                    value={this.state.message}
                                    onChange={this.handleChange('message')}
                                    margin="normal"
                                    variant="outlined"
                                />

                                <Button disabled={!this.state.message.length} className={classes.button} variant="contained" color="secondary" onClick={this.send}>Send</Button>

                            </div>
                        </Modal>
                    </div>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(SearchUser);

