import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import Sidebar from '../Sidebar';
import {
    Table, TableCell, TableRow, TableBody, TableHead
} from '@material-ui/core';

import { API } from 'aws-amplify';

const styles = theme => ({
    tableRoot: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    root: {
        flexGrow: 1,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
        overflow: 'auto',
        position: 'relative',
        display: 'flex',
        width: '100%',
        flexDirection: 'column'
    },
});

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
        padding: '16px 56px 16px 24px'
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

class Message extends Component {
    constructor() {
        super()

        this.state = {
            messages: {
                unRead: [],
                read: []
            }
        }
    }

    componentDidMount() {
        this.getMessages()
    }

    getMessages = () => {
        let apiName = 'TexHub';
        let path = `/message`;
        API.get(apiName, path)
            .then(response => {
                this.setState({
                    messages: response
                });
                if (response.unRead.length) {
                    this.markRead();
                }
            }).catch(error => {
                console.log(error.response)
            });
    }

    markRead = async () => {
        try {
            await API.post('TexHub', '/message/read', {
                body: { date: new Date().toISOString() }
            });
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const { classes } = this.props;
        const props = Object.assign({}, this.props);
        delete props.classes;

        const { messages } = this.state;

        return (
            <div className={classes.root}>
                <Sidebar {...props} />
                <div className={classes.content}>
                    <div className={classes.tableRoot}>
                        <h3>Unread Message</h3>

                        <Table className={classes.table}>
                            <TableHead>
                                <TableRow>
                                    <CustomTableCell>Date</CustomTableCell>
                                    <CustomTableCell>Message</CustomTableCell>
                                    <CustomTableCell>From</CustomTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    messages.unRead.map((p, i) => {
                                        return (
                                            <TableRow key={`message${i}`}>
                                                <TableCell>{p.createdAt}</TableCell>
                                                <TableCell>{p.message}</TableCell>
                                                <TableCell>{p.from.email}</TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </div>

                    <div className={classes.tableRoot}>
                        <h3>Read Message</h3>
                        <Table className={classes.table}>
                            <TableHead>
                                <TableRow>
                                    <CustomTableCell>Date</CustomTableCell>
                                    <CustomTableCell>Message</CustomTableCell>
                                    <CustomTableCell>From</CustomTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    messages.read.map((p, i) => {
                                        return (
                                            <TableRow key={`message${i}`}>
                                                <TableCell>{p.createdAt}</TableCell>
                                                <TableCell>{p.message}</TableCell>
                                                <TableCell>{p.from.email}</TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </div>
                </div>

            </div>
        );
    }
}

export default withStyles(styles)(Message);

