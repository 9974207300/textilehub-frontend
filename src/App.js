import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Amplify, { Auth } from 'aws-amplify';
import { withAuthenticator } from 'aws-amplify-react';

import './App.css';
import PubliRoutes from './router';
import awsmobile from './aws-exports';

Amplify.configure(awsmobile);
Amplify.configure({
  API: {
    endpoints: [
        {
            name: "TexHub",
            endpoint: "https://v6dpsbhl70.execute-api.us-east-1.amazonaws.com/dev",
            custom_header: async () => {
              const { idToken: { jwtToken }} = await Auth.currentSession();
              return { Authorization: jwtToken }
            }
        }
    ]
  } 
});

Amplify.configure({
  Auth: {
    region: 'us-east-1',
    userPoolId: 'us-east-1_64Jqp7BFB',
    userPoolWebClientId: '654bfhipnic4i9pqc7tdjctppd',
  }
});

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <PubliRoutes />
      </BrowserRouter>
    );
  }
}

const signUpConfig = {
  hideAllDefaults: true,
  signUpFields: [
    {
      label: 'Email',
      key: 'username',
      required: true,
      placeholder: 'Email',
      type: 'email',
      displayOrder: 1
    },
    {
      label: 'Password (Min length: 6)',
      key: 'password',
      required: true,
      placeholder: 'Password',
      type: 'password',
      displayOrder: 2
    }
  ]
};

export default withAuthenticator(App, {
  includeGreetings: true,
  signUpConfig
});
